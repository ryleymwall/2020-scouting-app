package com.example.scout882020.activities;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothServerSocket;
import android.bluetooth.BluetoothSocket;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.MenuItem;

import com.example.scout882020.R;
import com.example.scout882020.database.ScoutDB;
import com.example.scout882020.database.TeamMatchStatistics;
import com.google.android.material.bottomnavigation.BottomNavigationView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.navigation.NavController;
import androidx.navigation.NavDestination;
import androidx.navigation.Navigation;
import androidx.navigation.ui.AppBarConfiguration;
import androidx.navigation.ui.NavigationUI;
import androidx.room.Room;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.List;
import java.util.UUID;

public class ScouterMain extends AppCompatActivity {

    private DrawerLayout mDrawerLayout;
    BluetoothAdapter bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();

    private Handler handler;

    final String NAME = "bluered";
    final UUID MY_UUID = UUID.fromString("64e74f97-bf53-43d9-9058-6d99cbf04c4a");


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_scouter_main);
        BottomNavigationView navView = findViewById(R.id.bottom_nav_view);
        // Passing each menu ID as a set of Ids because each
        // menu should be considered as top level destinations.
        AppBarConfiguration appBarConfiguration = new AppBarConfiguration.Builder(
                R.id.navigation_pregame,
                R.id.navigation_auto,
                R.id.navigation_teleop,
                R.id.navigation_endgame)
                .build();
        NavController navController = Navigation.findNavController(this, R.id.nav_host_fragment);
        NavigationUI.setupActionBarWithNavController(this, navController, appBarConfiguration);
        NavigationUI.setupWithNavController(navView, navController);

        navController.addOnDestinationChangedListener(new NavController.OnDestinationChangedListener() {
            @Override
            public void onDestinationChanged(@NonNull NavController controller, @NonNull NavDestination destination, @Nullable Bundle arguments) {
                Log.v("navigation" , "changed");
            }
        });

        if (bluetoothAdapter.isEnabled()) {
            AcceptThread t = new AcceptThread();
            t.start();
        }


    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                mDrawerLayout.openDrawer(GravityCompat.START);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private class ConnectedThread extends Thread {
        private final BluetoothSocket mmSocket;
        private final InputStream mmInStream;
        private final OutputStream mmOutStream;
        private byte[] mmBuffer; // mmBuffer store for the stream

        public ConnectedThread(BluetoothSocket socket) {
            mmSocket = socket;
            InputStream tmpIn = null;
            OutputStream tmpOut = null;

            // Get the input and output streams; using temp objects because
            // member streams are final.
            try {
                tmpIn = socket.getInputStream();
            } catch (IOException e) {
                Log.e("bt_connection", "Error occurred when creating input stream", e);
            }
            try {
                tmpOut = socket.getOutputStream();
            } catch (IOException e) {
                Log.e("bt_connection", "Error occurred when creating output stream", e);
            }

            mmInStream = tmpIn;
            mmOutStream = tmpOut;
        }

        public void run() {
            mmBuffer = new byte[1024];
            int numBytes; // bytes returned from read()

            String msg = getStatsCSVasString();
            byte[] testMsg = msg.getBytes();
            write(testMsg);
        }

        // Call this from the main activity to send data to the remote device.
        public void write(byte[] bytes) {
            try {
                mmOutStream.write(bytes);

            } catch (IOException e) {
                Log.e("bt_connection", "Error occurred when sending data", e);

            }
        }

        // Call this method from the main activity to shut down the connection.
        public void cancel() {
            try {
                mmSocket.close();
            } catch (IOException e) {
                Log.e("bt_connection", "Could not close the connect socket", e);
            }
        }

        public String getStatsCSVasString(){
            final ScoutDB db = Room.databaseBuilder(getApplicationContext(), ScoutDB.class, "scout_db").build();

            List<TeamMatchStatistics> tal = db.m_dao().getAll();

            String csv = "";

            for(int i = 0; i < tal.size(); i++){
                csv = csv.concat(createCSVRow(tal.get(i)));
            }

            csv = csv.concat("@LL_D0N3!");

            return csv;

        }
        private String createCSVRow(TeamMatchStatistics tm){
            String comments = tm.comments.replace("\n" , " ").replace("\r" , " ");
            return tm.teamNumber + "," +
                    tm.matchNumber + "," +
                    tm.allianceMember + "," +
                    tm.autoOuterMakes + "," +
                    tm.autoLowerMakes + "," +
                    tm.autoOuterMisses + "," +
                    tm.teleopOuterMakes + "," +
                    tm.teleopLowerMakes + "," +
                    tm.teleopOuterMisses + "," +
                    tm.initiationLine + "," +
                    tm.positionControl + "," +
                    tm.rotationControl + "," +
                    tm.climbed + "," +
                    tm.parked + "," +
                    tm.balanced + "," +
                    comments + "," +
                    tm.playedDefense + "," +
                    tm.disconnected + "\n";
        }
    }

    private class createDBCSV extends AsyncTask<Void,Void,String> {

        @Override
        protected String doInBackground(Void... voids) {
            final ScoutDB db = Room.databaseBuilder(getApplicationContext(), ScoutDB.class, "scout_db").build();

            List<TeamMatchStatistics> tal = db.m_dao().getAll();

            String csv = "";

            for(int i = 0; i < tal.size(); i++){
                csv = csv.concat(createCSVRow(tal.get(i)));
            }

            return csv;
        }
    }

    private String createCSVRow(TeamMatchStatistics tm){
        return tm.teamNumber + "," +
                tm.matchNumber + "," +
                tm.allianceMember + "," +
                tm.autoOuterMakes + "," +
                tm.autoLowerMakes + "," +
                tm.autoOuterMisses + "," +
                tm.teleopOuterMakes + "," +
                tm.teleopLowerMakes + "," +
                tm.teleopOuterMisses + "," +
                tm.initiationLine + "," +
                tm.positionControl + "," +
                tm.rotationControl + "," +
                tm.climbed + "," +
                tm.comments + "," +
                tm.playedDefense + "," +
                tm.disconnected + "\n";
    }


    private class AcceptThread extends Thread {
        private final BluetoothServerSocket mmServerSocket;

        public AcceptThread() {
            // Use a temporary object that is later assigned to mmServerSocket
            // because mmServerSocket is final.
            BluetoothServerSocket tmp = null;
            try {
                // MY_UUID is the app's UUID string, also used by the client code.
                tmp = bluetoothAdapter.listenUsingRfcommWithServiceRecord(NAME, MY_UUID);
            } catch (IOException e) {
                Log.e("bluetooth", "Socket's listen() method failed", e);
            }
            mmServerSocket = tmp;
        }

        public void run() {
            BluetoothSocket socket = null;
            // Keep listening until exception occurs or a socket is returned.
            while (true) {
                try {
                    socket = mmServerSocket.accept();
                } catch (IOException e) {
                    Log.e("bluetooth", "Socket's accept() method failed", e);
                    break;
                }

                if (socket != null) {
                    // A connection was accepted. Perform work associated with
                    // the connection in a separate thread.
                    new ConnectedThread(socket).start();
                    Log.v("bluetooth" , "connected");
                    try {
                        mmServerSocket.close();
                    } catch (IOException e) {
                        Log.e("bluetooth", "Could not close the connect socket", e);
                    }
                    break;
                }
            }
        }

        // Closes the connect socket and causes the thread to finish.
        public void cancel() {
            try {
                mmServerSocket.close();
            } catch (IOException e) {
                Log.e("bluetooth", "Could not close the connect socket", e);
            }
        }
    }
}


