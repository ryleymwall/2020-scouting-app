package com.example.scout882020.activities;

import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;

import com.example.scout882020.database.ScoutDB;
import com.example.scout882020.database.TeamMatchStatistics;
import com.example.scout882020.database.TeamMatchStatsDao;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.room.Room;

import android.os.Message;
import android.os.ParcelFileDescriptor;
import android.provider.DocumentsContract;
import android.text.PrecomputedText;
import android.util.ArrayMap;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import com.example.scout882020.R;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.UUID;

public class BluetoothActivity extends AppCompatActivity {

    final int REQUEST_ENABLE_BT = 1;
    private static final int CREATE_FILE = 1;

    ArrayMap<String , String> macAddresses = new ArrayMap<>();
    ArrayMap<String , BluetoothDevice> btDevices = new ArrayMap<>();
    final UUID MY_UUID = UUID.fromString("64e74f97-bf53-43d9-9058-6d99cbf04c4a");
    final BluetoothAdapter bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();

    private final String CSV_HEADER = "team_number,match_number,scouter_name,driver_station,initiation_line,auto_outer_makes,auto_outer_misses,auto_lower_makes,teleop_lower_makes,teleop_outer_makes,teleop_outer_misses,rotation_control,position_control,parked,climbed,balanced,comments,played_defense,disconnected\n";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bluetooth);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        final Button blue1Btn = findViewById(R.id.blue_1_sync_btn);
        final Button blue2Btn = findViewById(R.id.blue_2_sync_btn);
        final Button blue3Btn = findViewById(R.id.blue_3_sync_btn);
        final Button red1Btn = findViewById(R.id.red_1_sync_btn);
        final Button red2Btn = findViewById(R.id.red_2_sync_btn);
        final Button red3Btn = findViewById(R.id.red_3_sync_btn);
        final Button downloadBtn = findViewById(R.id.download_csv_btn);

        //add mac Addresses of known tablets to list
        macAddresses.put("50:F5:DA:84:A2:65", "blue_1");
        macAddresses.put("F0:27:2D:0C:8D:91" , "blue_2");
        macAddresses.put("44:65:0D:9A:B5:85" , "blue_3");
        macAddresses.put("38:F7:3D:37:29:93" , "red_1");
        macAddresses.put("44:65:0D:01:BA:C6" , "red_2");
        macAddresses.put("44:65:0D:FA:E8:69" , "red_3");
        macAddresses.put("" , "ryley");

        if (!bluetoothAdapter.isEnabled()) {
            Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
            startActivityForResult(enableBtIntent, REQUEST_ENABLE_BT);
        }

        Set<BluetoothDevice> pairedDevices = bluetoothAdapter.getBondedDevices();

        if (pairedDevices.size() > 0) {
            // There are paired devices. Get the name and address of each paired device.
            for (BluetoothDevice device : pairedDevices) {
                String deviceName = device.getName();
                String deviceHardwareAddress = device.getAddress(); // MAC address
                if(macAddresses.containsKey(deviceHardwareAddress)){
                    String alliance_position = macAddresses.get(deviceHardwareAddress);
                    Log.v("bluetooth" , alliance_position);
                    btDevices.put(alliance_position , device);
                }
            }
        }

        blue1Btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ConnectThread ct = new ConnectThread(btDevices.get("blue_1") , blue1Btn);
                ct.start();
            }
        });

        blue2Btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ConnectThread ct = new ConnectThread(btDevices.get("blue_2") , blue2Btn);
                ct.start();
            }
        });

        blue3Btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ConnectThread ct = new ConnectThread(btDevices.get("blue_3") , blue3Btn);
                ct.start();
            }
        });

        red1Btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ConnectThread ct = new ConnectThread(btDevices.get("red_1") , red1Btn);
                ct.start();
            }
        });

        red2Btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ConnectThread ct = new ConnectThread(btDevices.get("red_2") , red2Btn);
                ct.start();
            }
        });

        red3Btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ConnectThread ct = new ConnectThread(btDevices.get("red_3") , red3Btn);
                ct.start();
            }
        });

        downloadBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                new GetStatsAsCSVString().execute();
                createFile();
            }
        });

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode,
                                 Intent resultData) {
        super.onActivityResult(requestCode, resultCode , resultData);
        if (requestCode == CREATE_FILE
                && resultCode == Activity.RESULT_OK) {
            // The result data contains a URI for the document or directory that
            // the user selected.
            Uri uri = null;
            if (resultData != null) {
                uri = resultData.getData();
                // Perform operations on the document using its URI.
                alterDocument(uri);
            }
        }
    }

    private void alterDocument(Uri uri) {
        try {
            ParcelFileDescriptor pfd = this.getContentResolver().
                    openFileDescriptor(uri, "w");
//            FileOutputStream fileOutputStream =
//                    new FileOutputStream(pfd.getFileDescriptor());
            new WriteToCSVFile().execute(pfd);

            // Let the document provider know you're done by closing the stream.
//            fileOutputStream.close();
//            pfd.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }



    private void createFile() {
        String filename = "data.csv";
        String fileContents = "Hello world!";
        try (FileOutputStream fos = getApplicationContext().openFileOutput(filename, Context.MODE_PRIVATE)) {
            fos.write(fileContents.getBytes());
        } catch (Exception e){
            Log.e("file_store" , "could not save file" , e);
        }
        Intent intent = new Intent(Intent.ACTION_CREATE_DOCUMENT);
        intent.addCategory(Intent.CATEGORY_OPENABLE);
        intent.setType("text/plain");
        intent.putExtra(Intent.EXTRA_TITLE, "data.csv");

        // Optionally, specify a URI for the directory that should be opened in
        // the system file picker when your app creates the document.
//        intent.putExtra(DocumentsContract.EXTRA_INITIAL_URI, pickerInitialUri);

        startActivityForResult(intent, CREATE_FILE);
    }
    public void addCSVtoDB(String csv){
        final ScoutDB db = Room.databaseBuilder(getApplicationContext(), ScoutDB.class, "scout_db").build();
        final TeamMatchStatsDao tmsDAO = db.m_dao();
        String[] csvRows = csv.split("\n");
        Log.v("databse" , "numRows: " + csvRows.length);
        for(String row : csvRows){
            try{
                String[] strData = row.split(",");
                TeamMatchStatistics data = new TeamMatchStatistics();
                data.teamNumber = Integer.parseInt(strData[0]);
                data.matchNumber = Integer.parseInt(strData[1]);
                data.allianceMember = strData[2];
                data.autoOuterMakes = Integer.parseInt(strData[3]);
                data.autoLowerMakes = Integer.parseInt(strData[4]);
                data.autoOuterMisses = Integer.parseInt(strData[5]);
                data.teleopOuterMakes = Integer.parseInt(strData[6]);
                data.teleopLowerMakes = Integer.parseInt(strData[7]);
                data.teleopOuterMisses = Integer.parseInt(strData[8]);
                data.initiationLine = Boolean.parseBoolean(strData[9]);
                data.positionControl = Boolean.parseBoolean(strData[10]);
                data.rotationControl = Boolean.parseBoolean(strData[11]);
                data.climbed = Boolean.parseBoolean(strData[12]);
                data.parked = Boolean.parseBoolean(strData[13]);
                data.balanced = Boolean.parseBoolean(strData[14]);
                data.comments = strData[15];
                data.playedDefense = Boolean.parseBoolean(strData[16]);
                data.disconnected = Boolean.parseBoolean(strData[17]);
                tmsDAO.insertMatchRecord(data);
            } catch (Exception e){
                Log.e("databse" , "could not add row" , e);
            }
        }
    }

    public void addCSVtoDB2(String csv){
        final ScoutDB db = Room.databaseBuilder(getApplicationContext(), ScoutDB.class, "scout_db").build();
        final TeamMatchStatsDao tmsDAO = db.m_dao();
//        String[] csvRows = csv.split("\n");
//        Log.v("databse" , "numRows: " + csvRows.length);
        String[] strData = csv.split(",");
        for (int i = 0; i < strData.length / 18; i++) {
            try {
//                    String[] strData = row.split(",");
                TeamMatchStatistics data = new TeamMatchStatistics();
                data.teamNumber = Integer.parseInt(strData[i+0]);
                data.matchNumber = Integer.parseInt(strData[i+1]);
                data.allianceMember = strData[i+2];
                data.autoOuterMakes = Integer.parseInt(strData[i+3]);
                data.autoLowerMakes = Integer.parseInt(strData[i+4]);
                data.autoOuterMisses = Integer.parseInt(strData[i+5]);
                data.teleopOuterMakes = Integer.parseInt(strData[i+6]);
                data.teleopLowerMakes = Integer.parseInt(strData[i+7]);
                data.teleopOuterMisses = Integer.parseInt(strData[i+8]);
                data.initiationLine = Boolean.parseBoolean(strData[i+9]);
                data.positionControl = Boolean.parseBoolean(strData[i+10]);
                data.rotationControl = Boolean.parseBoolean(strData[i+11]);
                data.climbed = Boolean.parseBoolean(strData[i+12]);
                data.parked = Boolean.parseBoolean(strData[i+13]);
                data.balanced = Boolean.parseBoolean(strData[i+14]);
                data.comments = strData[i+15];
                data.playedDefense = Boolean.parseBoolean(strData[i+16]);
                data.disconnected = Boolean.parseBoolean(strData[i+17]);
                tmsDAO.insertMatchRecord(data);
            } catch (Exception e) {
                Log.e("databse", "could not add row", e);
            }
        }

    }

    private class GetStatsAsCSVString extends AsyncTask<Void , Void , String> {

        @Override
        protected String doInBackground(Void... voids){
            final ScoutDB db = Room.databaseBuilder(getApplicationContext(), ScoutDB.class, "scout_db").build();

            List<TeamMatchStatistics> tal = db.m_dao().getAll();
            String csv = "";
            for(int i = 0; i < tal.size(); i++){
                csv = csv.concat(createCSVRow(tal.get(i)));
            }
            Log.v("databse" , csv);
            return csv;
        }

    }

    private class WriteToCSVFile extends AsyncTask<ParcelFileDescriptor , Void , Integer> {

        @Override
        protected Integer doInBackground(ParcelFileDescriptor... pfd){
            FileOutputStream fos =
                    new FileOutputStream(pfd[0].getFileDescriptor());

            final ScoutDB db = Room.databaseBuilder(getApplicationContext(), ScoutDB.class, "scout_db").build();

            List<TeamMatchStatistics> tal = db.m_dao().getAll();
//            String csv = CSV_HEADER;
            try{
                fos.write(CSV_HEADER.getBytes());
                for(int i = 0; i < tal.size(); i++){
                    fos.write(createCSVRow(tal.get(i)).getBytes());
//                    csv = csv.concat(createCSVRow(tal.get(i)));
                }
                fos.close();
                pfd[0].close();
            } catch (Exception e){
                Log.e("file_store" , "write to csv failed" , e);
            }

//            Log.v("databse" , csv);
            return new Integer(1);
        }

    }

//    public String getStatsCSVasString(){
//        final ScoutDB db = Room.databaseBuilder(getApplicationContext(), ScoutDB.class, "scout_db").build();
//        List<TeamMatchStatistics> tal = db.m_dao().getAll();
//        String csv = "";
//        for(int i = 0; i < tal.size(); i++){
//            csv = csv.concat(createCSVRow(tal.get(i)));
//        }
//        return csv;
//    }
    private String createCSVRow(TeamMatchStatistics tm){
        String res;
        res = tm.teamNumber + "," +
                tm.matchNumber + "," +
                tm.scouterName + "," +
                tm.allianceMember + ",";
        if(tm.initiationLine){
            res += "1,";
        }else{
            res += "0,";
        }
        res += tm.autoOuterMakes + "," +
                tm.autoOuterMisses + "," +
                tm.autoLowerMakes + "," +
                tm.teleopLowerMakes + "," +
                tm.teleopOuterMakes + "," +
                tm.teleopOuterMisses + ",";
        if(tm.rotationControl){
            res += "1,";
        }else{
            res += "0,";
        }
        if(tm.positionControl){
            res += "1,";
        }else{
            res += "0,";
        }
        if(tm.parked){
            res += "1,";
        }else{
            res += "0,";
        }
        if(tm.climbed){
            res += "1,";
        }else{
            res += "0,";
        }
        if(tm.balanced){
            res += "1,";
        }else{
            res += "0,";
        }
        res += tm.comments.replace("\n" , " ") + ",";
        if(tm.playedDefense){
            res += "1,";
        }else{
            res += "0,";
        }
        if(tm.disconnected){
            res += "1";
        }else{
            res += "0";
        }
        res += "\n";
        return res;
    }


    private class ConnectThread extends Thread {
        private final BluetoothSocket mmSocket;
        private final BluetoothDevice mmDevice;
        private final Button mmButton;

        public ConnectThread(BluetoothDevice device , Button mBtn) {
            // Use a temporary object that is later assigned to mmSocket
            // because mmSocket is final.
            BluetoothSocket tmp = null;
            mmDevice = device;
            mmButton = mBtn;

            try {
                // Get a BluetoothSocket to connect with the given BluetoothDevice.
                // MY_UUID is the app's UUID string, also used in the server code.
                tmp = device.createRfcommSocketToServiceRecord(MY_UUID);
            } catch (IOException e) {
                Log.e("bluetooth", "Socket's create() method failed", e);
            }
            mmSocket = tmp;
        }

        public void run() {
            Log.v("bluetooth" , "thread running");
            // Cancel discovery because it otherwise slows down the connection.
            bluetoothAdapter.cancelDiscovery();

            try {
                // Connect to the remote device through the socket. This call blocks
                // until it succeeds or throws an exception.
                Log.v("bluetooth" , "trying to connect");
                mmSocket.connect();
            } catch (IOException connectException) {
                Log.v("bluetooth" , "failed to connect");
                // Unable to connect; close the socket and return.
                try {
                    mmSocket.close();
                } catch (IOException closeException) {
                    Log.e("bluetooth", "Could not close the client socket", closeException);
                }
                return;
            }

            // The connection attempt succeeded. Perform work associated with
            // the connection in a separate thread.
            new ConnectedThread(mmSocket , mmButton).start();
            Log.v("bluetooth" , "connected");
            mmButton.setText("Connected");

        }

        // Closes the client socket and causes the thread to finish.
        public void cancel() {
            try {
                mmSocket.close();
            } catch (IOException e) {
                Log.e("bluetooth", "Could not close the client socket", e);
            }
        }
    }
    private class ConnectedThread extends Thread {
        private final BluetoothSocket mmSocket;
        private final InputStream mmInStream;
        private final OutputStream mmOutStream;
        private byte[] mmBuffer; // mmBuffer store for the stream
        private Button mmButton;

        public ConnectedThread(BluetoothSocket socket , Button mBtn) {
            mmSocket = socket;
            InputStream tmpIn = null;
            OutputStream tmpOut = null;
            mmButton = mBtn;

            // Get the input and output streams; using temp objects because
            // member streams are final.
            try {
                tmpIn = socket.getInputStream();
            } catch (IOException e) {
                Log.e("bt_connection", "Error occurred when creating input stream", e);
            }
            try {
                tmpOut = socket.getOutputStream();
            } catch (IOException e) {
                Log.e("bt_connection", "Error occurred when creating output stream", e);
            }

            mmInStream = tmpIn;
            mmOutStream = tmpOut;
        }

        public void run() {
            int chunkSize = 20000;
            mmBuffer = new byte[chunkSize];
            int numBytes; // bytes returned from read()
            String messageReceived = "";

            // Keep listening to the InputStream until an exception occurs.
            while (true) {
                try {
                    // Read from the InputStream.
                    while(true) {
                        numBytes = mmInStream.read(mmBuffer);
//                        if (numBytes == 0) {
//                            break;
//                        }
                        if(numBytes != chunkSize){
                            mmBuffer = Arrays.copyOf(mmBuffer , numBytes);
                        }
                        String message = new String(mmBuffer);
                        messageReceived = messageReceived.concat(message);
                        Log.v("bt_connection" , message);
                        if(message.contains("@LL_D0N3!")){
                            break;
                        }
                    }
                    mmButton.setText("Got Data");
//                    String message = new String(mmBuffer);
                    Log.v("bt_connection" , messageReceived);
                    addCSVtoDB(messageReceived);
                    break;

                } catch (IOException e) {
                    Log.d("bt_connection", "Input stream was disconnected", e);
//                    addCSVtoDB(messageReceived);
//                    mmButton.setText("Done");
                    break;
                }
            }
            Log.d("bt_connection", "Input stream was disconnected");
//            addCSVtoDB(messageReceived);
//            mmButton.setText("Done");

        }


        // Call this method from the main activity to shut down the connection.
        public void cancel() {
            try {
                mmSocket.close();
            } catch (IOException e) {
                Log.e("bt_connection", "Could not close the connect socket", e);
            }
        }
    }

}
