package com.example.scout882020.database;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;

import java.util.List;

@Dao
public interface TeamMatchStatsDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertMatchRecord(TeamMatchStatistics tms);

    @Query("SELECT * FROM TeamMatchStatistics WHERE teamNumber= :tn AND matchNumber= :mn")
    TeamMatchStatistics getStatsByPk(int tn , int mn);

    @Query("SELECT * FROM TeamMatchStatistics")
    List<TeamMatchStatistics> getAll();

    @Delete
    void deleteRecord(TeamMatchStatistics tms);

}
