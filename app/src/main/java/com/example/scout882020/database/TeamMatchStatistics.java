package com.example.scout882020.database;


import androidx.room.Entity;

@Entity(primaryKeys = {"teamNumber","matchNumber"})
public class TeamMatchStatistics {
    public int teamNumber;
    public int matchNumber;

    public String scouterName;
    public String allianceMember;

    //Autonomous Data
    public boolean initiationLine;
    public int autoOuterMisses;
    public int autoOuterMakes;
    public int autoLowerMakes;

    //Teleop Data
    public int teleopLowerMakes;
    public int teleopOuterMakes;
    public int teleopOuterMisses;
    public boolean rotationControl;
    public boolean positionControl;

    //Endgame
    public boolean parked;
    public boolean climbed;
    public boolean balanced;

    //Post Match
    public String comments;
    public boolean playedDefense;
    public boolean disconnected;

}
