package com.example.scout882020.database;

import androidx.room.Database;
import androidx.room.RoomDatabase;


@Database(entities = {TeamMatchStatistics.class} , version=1)
public abstract class ScoutDB extends RoomDatabase {
    public abstract TeamMatchStatsDao m_dao();
}
