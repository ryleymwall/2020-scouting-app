package com.example.scout882020.ui;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.Fragment;
import androidx.preference.Preference;
import androidx.preference.PreferenceManager;

import com.example.scout882020.R;
import com.example.scout882020.activities.BluetoothActivity;
import com.example.scout882020.activities.ScouterMain;
import com.example.scout882020.activities.SettingsActivity;

public class PregameFragment extends Fragment {

    private String [] TEAM_NUMBERS = {
            "23",
            "69",
            "78",
            "88",
            "97",
            "121",
            "126",
            "138",
            "172",
            "190",
            "246",
            "1757",
            "1768",
            "1786",
            "1922",
            "2079",
            "2168",
            "2262",
            "2342",
            "3205",
            "3958",
            "4048",
            "4151",
            "4169",
            "4564",
            "4908",
            "5000",
            "5846",
            "6201",
            "6301",
            "6324",
            "6329",
            "6529",
            "6620",
            "6731",
            "7164",
            "8013",
            "8023"
    };


    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {

        View root = inflater.inflate(R.layout.fragment_pregame, container, false);

        final EditText scouterNameTxt = root.findViewById(R.id.scouter_name_edit);
        final AutoCompleteTextView teamNumberTxt = root.findViewById(R.id.team_number_edit);
        final EditText matchNumberTxt = root.findViewById(R.id.match_number_edit);
        final ImageView settingsBtn = root.findViewById(R.id.setting_btn);
        final TextView allianceTxt = root.findViewById(R.id.alliance_member_text);
        final ImageView bluetoothSettingsBtn = root.findViewById(R.id.bt_setting_btn);

        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(getActivity());
        String alliance = preferences.getString("alliance_member" , "blue_1");
        alliance = alliance.replace('_',' ');
        alliance = alliance.toUpperCase();

        //Load state from singleton
        scouterNameTxt.setText(PerformanceState.getInstance().getScouterName());
        if(!(PerformanceState.getInstance().getTeamNumber() <= 0)){
            teamNumberTxt.setText(Integer.toString(PerformanceState.getInstance().getTeamNumber()));
        }
        if(!(PerformanceState.getInstance().getMatchNumber() <= 0)){
            matchNumberTxt.setText(Integer.toString(PerformanceState.getInstance().getMatchNumber()));
        }

        allianceTxt.setText(alliance);
        if(alliance.contains("B")){
            allianceTxt.setTextColor(getResources().getColor(R.color.blue));
        }
        else{
            allianceTxt.setTextColor(getResources().getColor(R.color.deleteRed));
        }

        ArrayAdapter<String> teamNumAdapter = new ArrayAdapter<>(getContext() , android.R.layout.simple_list_item_1 , TEAM_NUMBERS);
        teamNumberTxt.setThreshold(0);
        teamNumberTxt.setAdapter(teamNumAdapter);

        scouterNameTxt.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                String name = s.toString().replace(',','.');
                PerformanceState.getInstance().setScouterName(name);
            }
        });

        teamNumberTxt.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                try{
                    int tn = Integer.parseInt(s.toString());
                    PerformanceState.getInstance().setTeamNumber(tn);
                }catch (Exception e){

                }

            }
        });

        matchNumberTxt.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                try{
                    int mn = Integer.parseInt(s.toString());
                    PerformanceState.getInstance().setMatchNumber(mn);
                }catch(Exception e){
                    
                }

            }
        });

        settingsBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), SettingsActivity.class);
                startActivity(intent);
            }
        });

        bluetoothSettingsBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), BluetoothActivity.class);
                startActivity(intent);
            }
        });

        return root;
    }
}