package com.example.scout882020.ui;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.ToggleButton;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.preference.PreferenceManager;
import androidx.room.Room;

import com.example.scout882020.R;
import com.example.scout882020.activities.ScouterMain;
import com.example.scout882020.database.ScoutDB;
import com.example.scout882020.database.TeamMatchStatistics;
import com.google.android.material.bottomnavigation.BottomNavigationView;

import java.util.List;

public class EndgameFragment extends Fragment {

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {

        View root = inflater.inflate(R.layout.fragment_endgame, container, false);


        final ImageView generatorSwitchBtn = root.findViewById(R.id.generator_switch);
        final TextView generatorSwitchTxt = root.findViewById(R.id.generator_switch_txt);
        final ImageView climbBtn = root.findViewById(R.id.robot_climbing);
        final TextView climbTxt = root.findViewById(R.id.climb_txt);
        final EditText notesEdit = root.findViewById(R.id.review_notes);
        final ToggleButton defenseBtn = root.findViewById(R.id.defense_btn);
        final ToggleButton brokenBtn = root.findViewById(R.id.broken_btn);
        final Button submitBtn = root.findViewById(R.id.submit_btn);

        final ViewGroup.MarginLayoutParams climbMarginParams = (ViewGroup.MarginLayoutParams) climbBtn.getLayoutParams();
        final BottomNavigationView bnv = getActivity().findViewById(R.id.bottom_nav_view);

        //Load state from singleton
        if(PerformanceState.getInstance().isClimbed()){
            generatorSwitchBtn.setAlpha((float)1.0);
            if(PerformanceState.getInstance().isBalanced()){
                generatorSwitchTxt.setTextColor(getResources().getColor(R.color.addGreen));
                generatorSwitchTxt.setText("Balanced");
                generatorSwitchBtn.setRotation((float)180);
            }
            else{
                generatorSwitchTxt.setTextColor(getResources().getColor(R.color.deleteRed));
                generatorSwitchTxt.setText("Not Balanced");
                generatorSwitchBtn.setRotation((float)175);
            }
            climbMarginParams.topMargin = 32;
            climbTxt.setText("Climbed");
            climbTxt.setTextColor(getResources().getColor(R.color.addGreen));
        }
        else if(PerformanceState.getInstance().isParked()){
            climbMarginParams.topMargin = 72;
            climbTxt.setText("Parked");
            climbTxt.setTextColor(getResources().getColor(R.color.addGreen));
            generatorSwitchBtn.setAlpha((float)0.5);
        }
        else{
            climbMarginParams.topMargin = 72;
            climbTxt.setText("Not Parked");
            climbTxt.setTextColor(getResources().getColor(R.color.deleteRed));
            generatorSwitchBtn.setAlpha((float)0.5);
        }
        notesEdit.setText(PerformanceState.getInstance().getComments());
        defenseBtn.setChecked(PerformanceState.getInstance().isPlayedDefense());
        brokenBtn.setChecked(PerformanceState.getInstance().isDisconnected());


        generatorSwitchBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(PerformanceState.getInstance().isClimbed()){
                    PerformanceState.getInstance().setBalanced(!PerformanceState.getInstance().isBalanced());
                    if(PerformanceState.getInstance().isBalanced()){
                        generatorSwitchTxt.setTextColor(getResources().getColor(R.color.addGreen));
                        generatorSwitchTxt.setText("Balanced");
                        generatorSwitchBtn.setRotation((float)180);
                    }
                    else{
                        generatorSwitchTxt.setTextColor(getResources().getColor(R.color.deleteRed));
                        generatorSwitchTxt.setText("Not Balanced");
                        generatorSwitchBtn.setRotation((float)175);
                    }
                }
            }
        });

        climbBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PerformanceState.getInstance().toggleClimbMode();
                if(PerformanceState.getInstance().isClimbed()){
                    climbMarginParams.topMargin = 32;
                    climbTxt.setText("Climbed");
                    climbTxt.setTextColor(getResources().getColor(R.color.addGreen));
                    generatorSwitchBtn.setAlpha((float)1.0);
                    if(PerformanceState.getInstance().isBalanced()){
                        generatorSwitchTxt.setText("Balanced");
                        generatorSwitchTxt.setTextColor(getResources().getColor(R.color.addGreen));
                    }
                    else{
                        generatorSwitchTxt.setText("Not Balanced");
                        generatorSwitchTxt.setTextColor(getResources().getColor(R.color.deleteRed));
                    }
                }
                else if(PerformanceState.getInstance().isParked()){
                    climbMarginParams.topMargin = 72;
                    climbTxt.setText("Parked");
                    climbTxt.setTextColor(getResources().getColor(R.color.addGreen));
                    generatorSwitchBtn.setAlpha((float)0.5);
                    generatorSwitchTxt.setText("Balanced?");
                    generatorSwitchTxt.setTextColor(getResources().getColor(R.color.white));
                }
                else{
                    climbMarginParams.topMargin = 72;
                    climbTxt.setText("Not Parked");
                    climbTxt.setTextColor(getResources().getColor(R.color.deleteRed));
                    generatorSwitchBtn.setAlpha((float)0.5);
                    generatorSwitchTxt.setTextColor(getResources().getColor(R.color.white));
                    generatorSwitchTxt.setText("Balanced?");
                }
            }
        });

        notesEdit.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                PerformanceState.getInstance().setComments(notesEdit.getText().toString()
                        .replace(',','.')
                        .replace('\n' , ' '));
            }
        });

        defenseBtn.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                PerformanceState.getInstance().setPlayedDefense(isChecked);
            }
        });

        brokenBtn.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                PerformanceState.getInstance().setDisconnected(isChecked);
            }
        });

        submitBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(getActivity());
                String alliance = preferences.getString("alliance_member" , "blue_1");
                PerformanceState.getInstance().setAllianceMember(alliance);
                new WriteDBRecord().execute(PerformanceState.getInstance().createRecord());
                new WriteDebugDB().execute();
                String scouterName = PerformanceState.getInstance().getScouterName();
                int matchNumber = PerformanceState.getInstance().getMatchNumber() +1;
                PerformanceState.getInstance().clearPerformanceData();
                PerformanceState.getInstance().setMatchNumber(matchNumber);
                PerformanceState.getInstance().setScouterName(scouterName);
                bnv.setSelectedItemId(R.id.navigation_pregame);
            }
        });


        return root;
    }

    private String matchRecordtoString(TeamMatchStatistics tm){
        return tm.teamNumber + "\n" +
                tm.matchNumber + "\n" +
                tm.autoOuterMakes + "\n" +
                tm.autoLowerMakes + "\n" +
                tm.autoOuterMisses + "\n" +
                tm.teleopOuterMakes + "\n" +
                tm.teleopLowerMakes + "\n" +
                tm.teleopOuterMisses + "\n" +
                tm.initiationLine + "\n" +
                tm.positionControl + "\n" +
                tm.rotationControl + "\n" +
                tm.climbed + "\n" +
                tm.comments + "\n" +
                tm.playedDefense + "\n" +
                tm.disconnected + "\n";
    }

    private class WriteDBRecord extends AsyncTask<TeamMatchStatistics , Void , Integer> {

        @Override
        protected Integer doInBackground(TeamMatchStatistics... tms){
            final ScoutDB db = Room.databaseBuilder(getActivity().getApplicationContext(), ScoutDB.class, "scout_db").build();

            for(int i = 0; i < tms.length; i++) db.m_dao().insertMatchRecord(tms[i]);

            return new Integer(0);
        }

    }

    private class WriteDebugDB extends AsyncTask<Void,Void,Void>{

        @Override
        protected Void doInBackground(Void... voids) {
            final ScoutDB db = Room.databaseBuilder(getActivity().getApplicationContext(), ScoutDB.class, "scout_db").build();

            List<TeamMatchStatistics> tal = db.m_dao().getAll();
            Log.v("databse" , Integer.toString(tal.size()));

//            for(int i = 0; i < tal.size(); i++){
//                Log.v("databse" , matchRecordtoString(tal.get(i)));
//            }

            return null;
        }
    }

}


