package com.example.scout882020.ui;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.ToggleButton;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;


import com.example.scout882020.R;


public class AutoFragment extends Fragment {

    private final String MISSED_GOAL_TEXT = "Missed Goal: ";

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {

        View root = inflater.inflate(R.layout.fragment_auto, container, false);

        final TextView outerGoalText = root.findViewById(R.id.outer_goal_text);
        final ImageView addOuterGoalBtn = root.findViewById(R.id.outer_goal_add);
        final ImageView deleteOuterGoalBtn = root.findViewById(R.id.outer_goal_delete);

        final TextView missedGoalText = root.findViewById(R.id.missed_shots_txt);
        final ImageView addMissedBtn = root.findViewById(R.id.missed_add_btn);
        final ImageView deleteMissedBtn = root.findViewById(R.id.missed_delete_btn);

        final TextView lowerGoalText = root.findViewById(R.id.lower_goal_text);
        final ImageView addLowerGoalBtn = root.findViewById(R.id.lower_goal_add);
        final ImageView deleteLowerGoalBtn = root.findViewById(R.id.lower_goal_delete);

        final ToggleButton initiationLineBtn = root.findViewById(R.id.init_line_btn);

        //Load State from singleton
        outerGoalText.setText(Integer.toString(PerformanceState.getInstance().getAutoOuterMakes()));
        String tempMissedGoalText = MISSED_GOAL_TEXT + PerformanceState.getInstance().getAutoOuterMisses();
        missedGoalText.setText(tempMissedGoalText);
        lowerGoalText.setText(Integer.toString(PerformanceState.getInstance().getAutoLowerMakes()));
        initiationLineBtn.setChecked(PerformanceState.getInstance().isInitiationLine());


        addOuterGoalBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PerformanceState.getInstance().setAutoOuterMakes(PerformanceState.getInstance().getAutoOuterMakes() + 1);
                outerGoalText.setText(Integer.toString(PerformanceState.getAutoOuterMakes()));
            }
        });
        deleteOuterGoalBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!(PerformanceState.getInstance().getAutoOuterMakes()<= 0)){
                    PerformanceState.getInstance().setAutoOuterMakes(PerformanceState.getInstance().getAutoOuterMakes() -1);
                    outerGoalText.setText(Integer.toString(PerformanceState.getAutoOuterMakes()));
                }
            }
        });

        addLowerGoalBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PerformanceState.getInstance().setAutoLowerMakes(PerformanceState.getInstance().getAutoLowerMakes() + 1);
                lowerGoalText.setText(Integer.toString(PerformanceState.getAutoLowerMakes()));
            }
        });
        deleteLowerGoalBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!(PerformanceState.getInstance().getAutoLowerMakes()<= 0)){
                    PerformanceState.getInstance().setAutoLowerMakes(PerformanceState.getInstance().getAutoLowerMakes() -1);
                    lowerGoalText.setText(Integer.toString(PerformanceState.getAutoLowerMakes()));
                }
            }
        });

        addMissedBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PerformanceState.getInstance().setAutoOuterMisses(PerformanceState.getInstance().getAutoOuterMisses() + 1);
                String tempText = MISSED_GOAL_TEXT + PerformanceState.getAutoOuterMisses();
                missedGoalText.setText(tempText);
            }
        });
        deleteMissedBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!(PerformanceState.getInstance().getAutoOuterMisses()<= 0)){
                    PerformanceState.getInstance().setAutoOuterMisses(PerformanceState.getInstance().getAutoOuterMisses() -1);
                    String tempText = MISSED_GOAL_TEXT + PerformanceState.getAutoOuterMisses();
                    missedGoalText.setText(tempText);
                }
            }
        });

        initiationLineBtn.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                PerformanceState.getInstance().setInitiationLine(isChecked);
            }
        });



        return root;
    }
}