package com.example.scout882020.ui;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import com.example.scout882020.R;


public class TeleopFragment extends Fragment {

    private final String MISSED_GOAL_TEXT = "Missed Goal: ";

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_teleop, container, false);

        final TextView outerGoalText = root.findViewById(R.id.outer_goal_text);
        final ImageView addOuterGoalBtn = root.findViewById(R.id.outer_goal_add);
        final ImageView deleteOuterGoalBtn = root.findViewById(R.id.outer_goal_delete);

        final TextView missedGoalText = root.findViewById(R.id.missed_shots_txt);
        final ImageView addMissedBtn = root.findViewById(R.id.missed_add_btn);
        final ImageView deleteMissedBtn = root.findViewById(R.id.missed_delete_btn);

        final TextView lowerGoalText = root.findViewById(R.id.lower_goal_text);
        final ImageView addLowerGoalBtn = root.findViewById(R.id.lower_goal_add);
        final ImageView deleteLowerGoalBtn = root.findViewById(R.id.lower_goal_delete);

        final ImageView rotationControlBtn = root.findViewById(R.id.rotation_control_btn);
        final ImageView positionControlBtn = root.findViewById(R.id.position_control_btn);
        final TextView rotationControlText = root.findViewById(R.id.rotation_control_txt);
        final TextView positionControlText = root.findViewById(R.id.position_control_text);

        //Load State from singleton
        outerGoalText.setText(Integer.toString(PerformanceState.getInstance().getTeleopOuterMakes()));
        String tempMissedGoalText = MISSED_GOAL_TEXT + PerformanceState.getInstance().getTeleopOuterMisses();
        missedGoalText.setText(tempMissedGoalText);
        lowerGoalText.setText(Integer.toString(PerformanceState.getInstance().getTeleopLowerMakes()));
        if(PerformanceState.getInstance().isRotationControl()){
            rotationControlText.setTextColor(getResources().getColor(R.color.addGreen));
        }
        else{
            rotationControlText.setTextColor(getResources().getColor(R.color.deleteRed));
        }
        if(PerformanceState.getInstance().isPositionControl()){
            positionControlText.setTextColor(getResources().getColor(R.color.addGreen));
        }
        else{
            positionControlText.setTextColor(getResources().getColor(R.color.deleteRed));
        }

        addOuterGoalBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PerformanceState.getInstance().setTeleopOuterMakes(PerformanceState.getInstance().getTeleopOuterMakes() + 1);
                outerGoalText.setText(Integer.toString(PerformanceState.getInstance().getTeleopOuterMakes()));
            }
        });

        deleteOuterGoalBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(PerformanceState.getInstance().getTeleopOuterMakes() > 0){
                    PerformanceState.getInstance().setTeleopOuterMakes(PerformanceState.getInstance().getTeleopOuterMakes() - 1);
                    outerGoalText.setText(Integer.toString(PerformanceState.getInstance().getTeleopOuterMakes()));
                }
            }
        });

        addLowerGoalBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PerformanceState.getInstance().setTeleopLowerMakes(PerformanceState.getInstance().getTeleopLowerMakes() + 1);
                lowerGoalText.setText(Integer.toString(PerformanceState.getInstance().getTeleopLowerMakes()));
            }
        });

        deleteLowerGoalBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(PerformanceState.getInstance().getTeleopLowerMakes() > 0){
                    PerformanceState.getInstance().setTeleopLowerMakes(PerformanceState.getInstance().getTeleopLowerMakes() - 1);
                    lowerGoalText.setText(Integer.toString(PerformanceState.getInstance().getTeleopLowerMakes()));
                }
            }
        });

        addMissedBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PerformanceState.getInstance().setTeleopOuterMisses(PerformanceState.getInstance().getTeleopOuterMisses() + 1);
                String tempMissedGoalText1 = MISSED_GOAL_TEXT + PerformanceState.getInstance().getTeleopOuterMisses();
                missedGoalText.setText(tempMissedGoalText1);
            }
        });


        deleteMissedBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(PerformanceState.getInstance().getTeleopOuterMisses() > 0){
                    PerformanceState.getInstance().setTeleopOuterMisses(PerformanceState.getInstance().getTeleopOuterMisses() - 1);
                    String tempMissedGoalText1 = MISSED_GOAL_TEXT + PerformanceState.getInstance().getTeleopOuterMisses();
                    missedGoalText.setText(tempMissedGoalText1);
                }
            }
        });

        rotationControlBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PerformanceState.getInstance().setRotationControl(!PerformanceState.getInstance().isRotationControl());
                if(PerformanceState.getInstance().isRotationControl()){
                    rotationControlText.setTextColor(getResources().getColor(R.color.addGreen));
                }
                else{
                    rotationControlText.setTextColor((getResources().getColor(R.color.deleteRed)));
                }
            }
        });

        positionControlBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PerformanceState.getInstance().setPositionControl(!PerformanceState.getInstance().isPositionControl());
                if(PerformanceState.getInstance().isPositionControl()){
                    positionControlText.setTextColor(getResources().getColor(R.color.addGreen));
                }
                else{
                    positionControlText.setTextColor((getResources().getColor(R.color.deleteRed)));
                }
            }
        });

        return root;
    }
}
