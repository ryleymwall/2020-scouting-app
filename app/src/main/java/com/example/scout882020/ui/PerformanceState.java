package com.example.scout882020.ui;

import com.example.scout882020.database.TeamMatchStatistics;

class PerformanceState {
    private static final PerformanceState ourInstance = new PerformanceState();

    //entries match database entries
    private static int teamNumber;
    private static int matchNumber;

    private static String scouterName;
    private static String allianceMember;

    private static boolean initiationLine;
    private static int autoLowerMakes;
    private static int autoOuterMakes;
    private static int autoOuterMisses;

    private static int teleopLowerMakes;
    private static int teleopOuterMakes;
    private static int teleopOuterMisses;

    private static boolean rotationControl;
    private static boolean positionControl;

    private static boolean climbed;
    private static boolean parked;
    private static boolean balanced;

    private static String comments;
    private static boolean playedDefense;
    private static boolean disconnected;

    static PerformanceState getInstance() {
        return ourInstance;
    }

    public static void setTeamNumber(int teamNumber){
        PerformanceState.teamNumber = teamNumber;
    }

    public static void setMatchNumber(int matchNumber) {
        PerformanceState.matchNumber = matchNumber;
    }

    public static void setScouterName(String scouterName) {
        PerformanceState.scouterName = scouterName;
    }

    public static void setAllianceMember(String alliance_member) {
        PerformanceState.allianceMember = alliance_member;
    }

    public static void setInitiationLine(boolean initiationLine) {
        PerformanceState.initiationLine = initiationLine;
    }

    public static void setAutoLowerMakes(int autoLowerMakes) {
        PerformanceState.autoLowerMakes = autoLowerMakes;
    }

    public static void setAutoOuterMakes(int autoOuterMakes) {
        PerformanceState.autoOuterMakes = autoOuterMakes;
    }

    public static void setAutoOuterMisses(int autoOuterMisses) {
        PerformanceState.autoOuterMisses = autoOuterMisses;
    }


    public static void setTeleopLowerMakes(int teleopLowerMakes) {
        PerformanceState.teleopLowerMakes = teleopLowerMakes;
    }

    public static void setTeleopOuterMakes(int teleopOuterMakes) {
        PerformanceState.teleopOuterMakes = teleopOuterMakes;
    }

    public static void setTeleopOuterMisses(int teleopOuterMisses) {
        PerformanceState.teleopOuterMisses = teleopOuterMisses;
    }

    public static void setRotationControl(boolean rotationControl) {
        PerformanceState.rotationControl = rotationControl;
    }

    public static void setPositionControl(boolean positionControl) {
        PerformanceState.positionControl = positionControl;
    }

    public static void setClimbed(boolean climbed) {
        PerformanceState.climbed = climbed;
    }

    public static void setParked(boolean parked) {
        PerformanceState.parked = parked;
    }

    public static void setBalanced(boolean balanced) {
        PerformanceState.balanced = balanced;
    }

    public static int getTeamNumber(){
        return teamNumber;
    }

    public static int getMatchNumber() {
        return matchNumber;
    }

    public static String getScouterName() {
        return scouterName;
    }

    public static String getAllianceMember() {
        return allianceMember;
    }

    public static boolean isInitiationLine() {
        return initiationLine;
    }

    public static int getAutoLowerMakes() {
        return autoLowerMakes;
    }

    public static int getAutoOuterMakes() {
        return autoOuterMakes;
    }

    public static int getAutoOuterMisses() {
        return autoOuterMisses;
    }

    public static int getTeleopLowerMakes() {
        return teleopLowerMakes;
    }

    public static int getTeleopOuterMakes() {
        return teleopOuterMakes;
    }

    public static int getTeleopOuterMisses() {
        return teleopOuterMisses;
    }

    public static boolean isRotationControl() {
        return rotationControl;
    }

    public static boolean isPositionControl() {
        return positionControl;
    }

    public static boolean isClimbed() {
        return climbed;
    }

    public static boolean isParked() {
        return parked;
    }

    public static boolean isBalanced() {
        return balanced;
    }

    public static String getComments() {
        return comments;
    }

    public static void setComments(String comments) {
        PerformanceState.comments = comments;
    }

    public static boolean isPlayedDefense() {
        return playedDefense;
    }

    public static void setPlayedDefense(boolean playedDefense) {
        PerformanceState.playedDefense = playedDefense;
    }

    public static boolean isDisconnected() {
        return disconnected;
    }

    public static void setDisconnected(boolean disconnected) {
        PerformanceState.disconnected = disconnected;
    }

    private PerformanceState() {
        clearPerformanceData();
    }

    public static void clearPerformanceData(){
        teamNumber = 0;
        matchNumber = 0;
        initiationLine = false;
        autoLowerMakes = 0;
        autoOuterMakes = 0;
        autoOuterMisses = 0;
        teleopLowerMakes = 0;
        teleopOuterMakes = 0;
        teleopOuterMisses = 0;
        rotationControl = false;
        positionControl = false;
        climbed = false;
        parked = false;
        balanced = false;
        comments = "";
        playedDefense = false;
        disconnected = false;
    }

    public static TeamMatchStatistics createRecord(){
        TeamMatchStatistics tms = new TeamMatchStatistics();
        tms.teamNumber = teamNumber;
        tms.matchNumber = matchNumber;
        tms.initiationLine = initiationLine;
        tms.autoOuterMakes = autoOuterMakes;
        tms.autoLowerMakes = autoLowerMakes;
        tms.autoOuterMisses = autoOuterMisses;
        tms.teleopOuterMakes = teleopOuterMakes;
        tms.teleopLowerMakes = teleopLowerMakes;
        tms.teleopOuterMisses = teleopOuterMisses;
        tms.rotationControl = rotationControl;
        tms.positionControl = positionControl;
        tms.climbed = climbed;
        tms.parked = parked;
        tms.balanced = climbed && balanced;
        tms.comments = comments;
        tms.playedDefense = playedDefense;
        tms.disconnected = disconnected;
        tms.allianceMember = allianceMember;
        return tms;
    }

    public void toggleClimbMode(){
        if(!(climbed || parked)){
            setParked(true);
        }
        else if(parked){
            setParked(false);
            setClimbed(true);
        }
        else if(climbed){
            setClimbed(false);
            setParked(false);
        }
    }
}
